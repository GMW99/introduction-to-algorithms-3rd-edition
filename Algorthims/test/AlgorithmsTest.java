/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gabryel
 */
public class AlgorithmsTest {
    private Algorithms algorithms;
    public AlgorithmsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
         algorithms = new Algorithms();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetIntArray() {
    }

    @Test
    public void testSetIntArray() {
    }

    @Test
    public void testInsertionSort() {
        System.out.println("Testing Insertion sort");
        int [][] test = {{1},{2,1},{4,1,4,5},{-6,10,8,-1},{0,-1,3}, {}};
        int [][] result = {{1}, {1,2},{1,4,4,5},{-6,-1,8,10},{-1,0,3},{}};        
        for (int count = 0; count < test.length; count++){
            algorithms.setIntArray(test[count]);
            Assert.assertArrayEquals("Sort "+ Arrays.toString(test[count])+ " Expected outcome: "+ Arrays.toString(result[count]),result[count],algorithms.InsertionSort(test[count]));
        }
    }

    @Test
    public void testMain() {
    }
    
    @Test
    // By testing mergeSort it tests Merge
    public void testMergeSort() {
        System.out.println("Testing Merge sort");
        int [][] test = {{1},{2,1},{4,1,4,5},{-6,10,8,-1},{0,-1,3},{}};
        int [][] result = {{1}, {1,2},{1,4,4,5},{-6,-1,8,10},{-1,0,3},{}};        
        for (int count = 0; count < test.length; count++){
            algorithms.setIntArray(test[count]);
            Assert.assertArrayEquals("Sort "+ Arrays.toString(test[count])
                    + " Expected outcome: "+ Arrays.toString(result[count]),
                    result[count],algorithms.MergeSort(test[count], 0,
                    test[count].length -1));
        }
    }

    @Test
    public void testMerge() {
    }

    @Test
    public void testLinearSearch() {
        System.out.println("Testing Linear Search");
        int [] [] test = {{1},{2,1},{4,1,4,5},{-6,10,8,-1},{0,-1,3},{}};
        int [] v = {1,3,5,8,-1,4};
        boolean [] result = {true,false,true,true,true,false};
        for (int count = 0; count < test.length; count++){
            algorithms.setIntArray(test[count]);
            Assert.assertEquals("Find "+ (v[count])
                    + " In "+ Arrays.toString(test[count])
                    + " Expected outcome: " + (result[count]),
                    result[count],algorithms.LinearSearch(test[count], v[count] ));
        }
    }

    @Test
    public void testBinarySort() {

    }

    @Test
    public void testBinarySearch() {
        System.out.println("Testing Binary Search");
        int [] [] test = {{1},{1,2},{1,4,4,5},{-6,-1,8,10},{-1,0,3}};
        int [] v = {1,3,5,8,-1};
        int [] a = {0,0,0,0,0};
        int [] b = {1,2,4,4,3};
        boolean [] result = {true,false,true,true,true,false};
        for (int count = 0; count < test.length ; count++){
            algorithms.setIntArray(test[count]);
            Assert.assertEquals("Find "+ (v[count])
                + " In "+ Arrays.toString(test[count])
                + " Expected outcome: " + (result[count]),
                result[count],algorithms.BinarySearch(test[count], v[count], a[count], b[count] ));
        }
    }

    @Test
    public void testBubbleSort() {
        System.out.println("Testing Bubble Sort");
        int [] [] test  = {{1},{2,1},{5,4,4,1},{8,-1,-6,10}, {3,0,-1},{}};
        int [] [] result = {{1},{1,2},{1,4,4,5},{-6,-1,8,10},{-1,0,3},{}};
        for (int count = 0; count < test.length ; count++){
            algorithms.setIntArray(test[count]);
            Assert.assertEquals("Sort "+ Arrays.toString(test[count])
                + " Expected outcome: " + Arrays.toString(result[count]),
            Arrays.toString(result[count]),Arrays.toString(algorithms.BubbleSort(test[count])));
        }
        
    }

    @Test
    public void testHornerRule() {
        System.out.println("Testing Horner Rule");
        int [] [] TestPoly  = {{1},{2,1},{5,4,4,1},{8,-1,-6,10}, {3,0,-1}};
        int [] TestX = {0,1,-1,1,2,4};
        int [] [] result = {{1},{3},{-4},{11},{11}};
        for (int count = 0; count < TestPoly.length ; count++){
            algorithms.setIntArray(TestPoly[count]);
            Assert.assertEquals("solve " + Arrays.toString(TestPoly[count])
                + " Expected outcome: " + Arrays.toString(result[count]),
            Arrays.toString(result[count]),Arrays.toString(algorithms.HornerRule(TestPoly[count], TestX[count])));
        }
    }
    
}
