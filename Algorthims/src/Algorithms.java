
import java.util.Arrays;
import sun.awt.resources.awt_pt_BR;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gabryel
 */
public class Algorithms {
    private int [] intArray;
    
    public Algorithms(){
        intArray = null;
    }
    public static void main(String[] args) {
        int [] a = {1,2,3,4,5};
        System.out.println((BinarySearch(a,0,0,5)));
    }
    public int [] getIntArray(){
        return intArray;
    }
    public void setIntArray(int []intArray){
        this.intArray = intArray;
    }
    /* efficent for small number of elements i.e a hand
    * hand of cards.
    */
    public static int [] InsertionSort(int[] array){
        int key;
        int i;
        for (int j = 0; j< array.length; j++ ){
            key = array[j];
            i = j-1;
            while (i>=0 && array[i]> key) {
                array[i+1] = array[i];
                i=i-1;
            }
            array[i+1] = key;
        }
        return array;
    }
    public static int [] MergeSort(int[] array, int p, int r){
        int q;
        if (p<r){
            q = (p+r)/2;
            MergeSort(array, p, q);
            MergeSort(array, q+1 , r);
            Merge(array, p, q, r);
        }
        return array;
    }
    public static int [] Merge(int[] array,int p, int q, int r){
        int n1 = q-p+1;
        int n2 = r-q;
        int [] L = new int [n1+1];
        int [] R = new int [n2+1];
        L[n1] = 100000;
        R[n2] = 100000;
        
        for (int i = 0; i < n1; i++ ){
            L[i] = array[p + i];
        }
        for (int i = 0; i < n2; i++){
            R[i] = array[q + i + 1];
        }
        int i =0;
        int j = 0;
        for(int k = p; k <= r; k++){
            if(L[i] <= R[j]){
                array[k] = L[i];
                i+=1;
            }
            else{
                array[k] = R[j];
                j+=1;
            }
        }
        return array;
    }
    public static boolean LinearSearch(int [] array, int v){
        for (int i = 0; i < array.length; i++) {
            if (v == array[i]){
                return  true;
            }    
        }
        return false;
    }
    
    public static boolean BinarySearch(int [] array, int v, int a , int b){ 
        int m;
        if ( a >= b){
            return false;
        }
        m = (a+b)/2;
        if (v == array[m]){
            return true;
        }
        if (m < v){
            return BinarySearch(array, v, m+1, b);
        }
        return BinarySearch(array, v, a, m);
    }
    
    public static int [] BubbleSort(int [] array){
        int [][] test = {{1},{2,1},{4,1,4,5},{-6,10,8,-1},{0,-1,3}, {}};
        int [][] result = {{1}, {1,2},{1,4,4,5},{-6,-1,8,10},{-1,0,3},{}};
		
		        for (int i = 0; i < array.length -1; i++){
            for (int j = array.length -1; j > 0 ; j--){
                if (array[j] < array[j-1]){
                    int temp;
                    temp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = temp;
                }
            }
        }
        return array;
    }
    
    public static int [] HornerRule(int [] A, int x){
        int [] y = {0};
        y[0] = A[0];
        for(int i = 1; i < A.length; i++){
            y[0] = y[0] *x + A[i];
        }
        
        return  y;
    }
}
